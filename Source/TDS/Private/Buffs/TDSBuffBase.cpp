// Created by Dmitriy Chepurkin, All Right Reserved

#include "Buffs/TDSBuffBase.h"

#include "TDSHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBuff, All, All);

//Base
void UTDSBuffBase::InitBuff(AActor* Target, AActor* Caster)
{
	BuffTarget = Target;
	BuffCaster = Caster;
	FXComponent = UGameplayStatics::SpawnEmitterAttached(FX, Target->GetRootComponent());
}

void UTDSBuffBase::DestroyBuff()
{
	OnBuffDestroy.Broadcast(this);
	ConditionalBeginDestroy();
}

//End Base

//Once Buff
void UTDSBuffOnce::InitBuff(AActor* Target, AActor* Caster)
{
	Super::InitBuff(Target, Caster);
	ApplyBuff();
	DestroyBuff();
}

void UTDSBuffOnce::ApplyBuff() const
{
	if(!BuffTarget) return;

	const auto HealthComponent = BuffTarget->FindComponentByClass<UTDSHealthComponent>();
	if(!HealthComponent) return;

	switch(BuffType)
	{
	case EBuffType::Damage:
		BuffTarget->TakeDamage(Power, FDamageEvent(), nullptr, BuffCaster);
		break;
	case EBuffType::Heal:
		HealthComponent->AddHealth(Power);
		break;
	default: ;
	}
}

//End Once Buff

//Tick Buff

void UTDSBuffTick::InitBuff(AActor* Target, AActor* Caster)
{
	UTDSBuffBase::InitBuff(Target, Caster);
	if(!GetWorld()) return;

	GetWorld()->GetTimerManager().SetTimer(LifeTimerHandle, this, &ThisClass::DestroyBuff, BuffTime);

	AOE
		? GetWorld()->GetTimerManager().SetTimer(TickTimerHandle, this, &ThisClass::MakeAOE, BuffTickRate, true)
		: GetWorld()->GetTimerManager().SetTimer(TickTimerHandle, this, &ThisClass::ApplyBuff, BuffTickRate, true);
}

void UTDSBuffTick::MakeAOE()
{
	BuffTarget->TakeDamage(Power, FDamageEvent(), nullptr, BuffCaster);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), Power, BuffTarget->GetActorLocation(), Radius, nullptr, {BuffCaster, BuffTarget}, BuffCaster, nullptr, true);
}

void UTDSBuffTick::DestroyBuff()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TickTimerHandle);
		GetWorld()->GetTimerManager().ClearTimer(LifeTimerHandle);
	}
	if(FXComponent) FXComponent->DestroyComponent();
	Super::DestroyBuff();
}

//End Tick Buff
