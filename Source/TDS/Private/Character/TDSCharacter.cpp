// Created by Dmitriy Chepurkin, All Right Reserved

#include "TDSCharacter.h"

#include "TDSBuffComponent.h"
#include "TDSCharacterStatsComponent.h"
#include "TDSInventoryComponent.h"
#include "TDSSpringArmComponent.h"
#include "TDSWeaponComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDSCharacter::ATDSCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UTDSMovementComponent>(CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->InitCapsuleSize(30.f, 68.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<UTDSSpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->bDoCollisionTest = false;

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, UTDSSpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	ShellMesh = CreateDefaultSubobject<UStaticMeshComponent>("ShellMesh");
	ShellMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ShellMesh->SetupAttachment(GetMesh(), "ShellSocket");

	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>("InventoryComponent");
	WeaponComponent = CreateDefaultSubobject<UTDSWeaponComponent>("WeaponComponent");
	StatsComponent = CreateDefaultSubobject<UTDSCharacterStatsComponent>("StatsComponent");
	BuffComponent = CreateDefaultSubobject<UTDSBuffComponent>("BuffComponent");
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ThisClass::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight", this, &ThisClass::InputAxisY);
	PlayerInputComponent->BindAxis("CameraHeight", CameraBoom, &UTDSSpringArmComponent::ChangeCameraHeight);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ThisClass::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ThisClass::StopFire);
	PlayerInputComponent->BindAction("ReloadWeapon", IE_Pressed, this, &ThisClass::ReloadWeapon);
	PlayerInputComponent->BindAction("DropWeapon", IE_Pressed, this, &ThisClass::DropWeapon);

	DECLARE_DELEGATE_OneParam(FOnMovementAction, const bool);
	PlayerInputComponent->BindAction<FOnMovementAction>("Aim", IE_Pressed, GetTDSCharacterMovement(), &UTDSMovementComponent::OnAimAction, true);
	PlayerInputComponent->BindAction<FOnMovementAction>("Aim", IE_Released, GetTDSCharacterMovement(), &UTDSMovementComponent::OnAimAction, false);
	PlayerInputComponent->BindAction<FOnMovementAction>("Walk", IE_Pressed, GetTDSCharacterMovement(), &UTDSMovementComponent::OnWalkAction, true);
	PlayerInputComponent->BindAction<FOnMovementAction>("Walk", IE_Released, GetTDSCharacterMovement(), &UTDSMovementComponent::OnWalkAction, false);
	PlayerInputComponent->BindAction<FOnMovementAction>("Sprint", IE_Pressed, this, &ThisClass::OnSprintAction, true);
	PlayerInputComponent->BindAction<FOnMovementAction>("Sprint", IE_Released, this, &ThisClass::OnSprintAction, false);

	DECLARE_DELEGATE_OneParam(FOnSetWeapon, const int32);
	PlayerInputComponent->BindAction<FOnSetWeapon>("SetWeapon1", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::SetWeapon, 0);
	PlayerInputComponent->BindAction<FOnSetWeapon>("SetWeapon2", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::SetWeapon, 1);
	PlayerInputComponent->BindAction<FOnSetWeapon>("SetWeapon3", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::SetWeapon, 2);
	PlayerInputComponent->BindAction<FOnSetWeapon>("SetWeapon4", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::SetWeapon, 3);
	PlayerInputComponent->BindAction<FOnSetWeapon>("SetWeapon5", IE_Pressed, InventoryComponent, &UTDSInventoryComponent::SetWeapon, 4);
}

void ATDSCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	CameraBoom->bEnableCameraLag = Flow;
	CameraBoom->CameraLagSpeed = CoefSmooth;
	CameraBoom->SetRelativeRotation(CameraRotation);
}

UAnimMontage* ATDSCharacter::GetDeathAnim() const
{
	if(DeathAnimations.Num() == 0) return nullptr;

	return DeathAnimations[FMath::RandHelper(DeathAnimations.Num() - 1)];
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(GetTDSCharacterMovement())
	{
		GetTDSCharacterMovement()->OnChangeMovementState.AddUObject(this, &ThisClass::OnChangeMovementState);
	}

	if(CursorDecalMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(this, CursorDecalMaterial, CursorDecalSize, FVector::ZeroVector);
	}

	if(WeaponComponent)
	{
		WeaponComponent->OnMakeShoot.AddUObject(this, &ThisClass::OnMakeShoot);
		WeaponComponent->OnReloadStart.AddUObject(this, &ThisClass::OnReloadStart);
		WeaponComponent->OnReloadEnd.AddUObject(this, &ThisClass::OnReloadEnd);
	}

	if(StatsComponent)
	{
		StatsComponent->OnStaminaOver.AddDynamic(this, &ThisClass::OnStaminaOver);
		StatsComponent->OnDeath.AddDynamic(this, &ThisClass::Death);
	}
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	CursorTick(DeltaSeconds);
}

void ATDSCharacter::CursorTick(const float DeltaSeconds)
{
	FHitResult HitResult;
	GetHitResultUnderCursor(HitResult);

	SetCursorTransform(HitResult);
	if(!IsSprint()) SetRotationToCursor(DeltaSeconds);
}

void ATDSCharacter::SetCursorTransform(const FHitResult& HitResult) const
{
	if(!CurrentCursor) return;

	const auto CursorFV = HitResult.ImpactNormal;
	const auto CursorR = CursorFV.Rotation();
	CurrentCursor->SetWorldLocation(HitResult.Location);
	CurrentCursor->SetWorldRotation(CursorR);
}

void ATDSCharacter::SetRotationToCursor(const float DeltaSeconds)
{
	if(bIsDead || !GetMesh()) return;

	const auto PlayerController = GetController<APlayerController>();
	if(!PlayerController) return;

	FVector MousePosition;
	PlayerController->GetMousePosition(MousePosition.X, MousePosition.Y);

	FVector2D CharacterScreenLocation2D;
	PlayerController->ProjectWorldLocationToScreen(GetActorLocation(), CharacterScreenLocation2D);
	const auto CharacterScreenLocation = FVector(CharacterScreenLocation2D, 0.f);

	auto TargetYawRotation = UKismetMathLibrary::FindLookAtRotation(CharacterScreenLocation, MousePosition);

	TargetYawRotation.Yaw -= GetMesh()->GetRelativeRotation().Yaw;
	const auto NewYawRotation = FMath::RInterpConstantTo(GetActorRotation(), TargetYawRotation, DeltaSeconds, RotationToCursorSpeed);
	SetActorRotation(NewYawRotation);
}

void ATDSCharacter::GetHitResultUnderCursor(FHitResult& HitResult) const
{
	const auto PlayerController = GetController<APlayerController>();
	if(!PlayerController) return;

	PlayerController->GetHitResultUnderCursorByChannel(TraceTypeQuery3, true, HitResult);
}

void ATDSCharacter::InputAxisX(const float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisValue);
}

void ATDSCharacter::InputAxisY(const float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisValue);
}

bool ATDSCharacter::IsSprint() const
{
	return GetTDSCharacterMovement() && GetTDSCharacterMovement()->IsSprint();
}

bool ATDSCharacter::IsMoving() const
{
	return GetTDSCharacterMovement() && GetTDSCharacterMovement()->IsMoving();
}

void ATDSCharacter::OnSprintAction(const bool SptintEnabled)
{
	if(!GetTDSCharacterMovement() || !StatsComponent) return;
	GetTDSCharacterMovement()->SetSprint(SptintEnabled);
	StatsComponent->SetUsingStamina(SptintEnabled);
}

void ATDSCharacter::OnStaminaOver()
{
	if(!GetTDSCharacterMovement()) return;
	GetTDSCharacterMovement()->SetSprint(false);
}

void ATDSCharacter::Death()
{
	bIsDead = true;
	StopFire();

	GetTDSCharacterMovement()->DisableMovement();
	if(BuffComponent) BuffComponent->DestroyComponent();
	OnDeath();
}

void ATDSCharacter::SetMovementState(const EMovementState NewMovementState)
{
	const auto TDSMovementComponent = GetTDSCharacterMovement();
	if(!TDSMovementComponent) return;

	TDSMovementComponent->SetMovementState(NewMovementState);
}

void ATDSCharacter::OnChangeMovementState(const EMovementState NewMovementState)
{
	if(NewMovementState == EMovementState::MS_SprintState)
	{
		StopFire();
	}
}

void ATDSCharacter::StartFire()
{
	if(!CanFire()) return;

	WeaponComponent->StartFire();
}

void ATDSCharacter::StopFire()
{
	if(!WeaponComponent) return;

	WeaponComponent->StopFire();
}

void ATDSCharacter::ReloadWeapon()
{
	if(!WeaponComponent) return;

	WeaponComponent->ReloadWeapon();
}

void ATDSCharacter::DropWeapon()
{
	if(!WeaponComponent || WeaponComponent->IsReload() || !InventoryComponent) return;

	InventoryComponent->DropWeapon();
}

bool ATDSCharacter::CanFire() const
{
	return WeaponComponent &&
		GetTDSCharacterMovement() &&
		GetTDSCharacterMovement()->GetMovementState() != EMovementState::MS_SprintState;
}

void ATDSCharacter::OnReloadEnd(const FName& WeaponTableName)
{
	StopAnimMontage();
}

void ATDSCharacter::OnMakeShoot(const FFireAnimationData& FireAnimations)
{
	if(!GetTDSCharacterMovement()) return;

	const auto FireAnimation = GetTDSCharacterMovement()->IsAim() ? FireAnimations.AimFireAnimation : FireAnimations.FireAnimation;
	PlayAnimMontage(FireAnimation);
}
