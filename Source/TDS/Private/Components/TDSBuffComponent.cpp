// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSBuffComponent.h"

#include "TDSBuffBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogBuffComponent, All, All);

UTDSBuffComponent::UTDSBuffComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UTDSBuffComponent::TryToAddBuff(TSubclassOf<UTDSBuffBase>& NewBuff, AActor* Caster)
{
	if(!GetOwner()) return;

	const auto FindedBuff = Buffs.FindByPredicate([&](const UTDSBuffBase* Buff)
	{
		return Buff->GetClass() == NewBuff;
	});
	if(FindedBuff) return;

	const auto DefaultBuffObject = NewBuff.GetDefaultObject();
	const auto Chance = FMath::RandRange(0.f, 1.f);
	if(Chance > DefaultBuffObject->GetChance()) return;

	const auto Buff = NewObject<UTDSBuffBase>(GetOwner(), NewBuff);
	if(!Buff) return;

	Buffs.Add(Buff);
	Buff->OnBuffDestroy.AddUObject(this, &ThisClass::DestroyBuff);
	Buff->InitBuff(GetOwner(), Caster);
}

void UTDSBuffComponent::DestroyBuff(UTDSBuffBase* DestroyedBuff)
{
	if(!Buffs.Contains(DestroyedBuff)) return;

	Buffs.Remove(DestroyedBuff);
}
