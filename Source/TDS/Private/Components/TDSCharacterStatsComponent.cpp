// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSCharacterStatsComponent.h"

void UTDSCharacterStatsComponent::BeginPlay()
{
	SetShield(MaxShield);
	SetStamina(MaxStamina);
	Super::BeginPlay();
}

void UTDSCharacterStatsComponent::AddShield(const float ShieldToAdd)
{
	SetShield(CurrentShield + ShieldToAdd);
}

void UTDSCharacterStatsComponent::AddStamina(const float StaminaToAdd)
{
	SetStamina(CurrentStamina + StaminaToAdd);
}

void UTDSCharacterStatsComponent::SetUsingStamina(const bool UsingEnabled)
{
	SetStaminaUseTimerEnabled(UsingEnabled);
	SetStaminaRecoveryTimerEnabled(!UsingEnabled);
}

void UTDSCharacterStatsComponent::SetShield(const float NewShield)
{
	CurrentShield = FMath::Clamp(NewShield, 0.f, MaxShield);
	OnChangeShield.Broadcast(CurrentShield, CurrentShield / MaxShield);
}

void UTDSCharacterStatsComponent::UseShield(const float ShieldToUse)
{
	SetShield(CurrentShield - ShieldToUse);
	if(IsShieldBroked())
	{
		OnBrokeShield.Broadcast();
	}
}

void UTDSCharacterStatsComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(FMath::IsNearlyZero(Damage)) return;

	auto TakedDamage = Damage * DamageCoef;
	auto HealthDamage = 0.f;

	if(TakedDamage > CurrentShield)
	{
		HealthDamage = (TakedDamage - CurrentShield) / DamageCoef;
		TakedDamage = CurrentShield;
	}

	if(!IsShieldBroked())
	{
		OnShieldTakeDamage.Broadcast(TakedDamage);
		UseShield(TakedDamage);
	}

	Super::TakeDamage(DamagedActor, HealthDamage, DamageType, InstigatedBy, DamageCauser);

	SetSheildTimerEnabled(!IsDead());
	if(IsDead())
	{
		SetStaminaRecoveryTimerEnabled(false);
	}
}

void UTDSCharacterStatsComponent::ShieldRecoveryTick()
{
	AddShield(ShieldRecovery);
	if(FMath::IsNearlyEqual(CurrentShield, MaxShield))
	{
		SetSheildTimerEnabled(false);
	}
}

void UTDSCharacterStatsComponent::SetSheildTimerEnabled(const bool Enabled)
{
	if(!GetWorld()) return;

	Enabled
		? GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryTimerHandle, this, &ThisClass::ShieldRecoveryTick, ShieldRecoveryRate, true,
		                                         ShieldRecoveryDelay)
		: GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimerHandle);
}

void UTDSCharacterStatsComponent::SetStamina(const float NewStamina)
{
	CurrentStamina = FMath::Clamp(NewStamina, 0.f, MaxStamina);
	OnChangeStamina.Broadcast(CurrentStamina, CurrentStamina / MaxStamina);
}

void UTDSCharacterStatsComponent::UseStamina(const float StaminaToUse)
{
	SetStamina(CurrentStamina - StaminaToUse);
	if(IsStaminaOver())
	{
		OnStaminaOver.Broadcast();
		SetUsingStamina(false);
	}
}

void UTDSCharacterStatsComponent::StaminaUseTick()
{
	if(GetOwner())
	{
		if(FMath::IsNearlyZero(GetOwner()->GetVelocity().Size()))
		{
			SetStaminaRecoveryTimerEnabled(true);
		}
		else
		{
			SetStaminaRecoveryTimerEnabled(false);
			UseStamina(StaminaUse);
		}
	}
}

void UTDSCharacterStatsComponent::StaminaRecoveryTick()
{
	AddStamina(StaminaRecovery);
	if(FMath::IsNearlyEqual(CurrentStamina, MaxStamina))
	{
		SetStaminaRecoveryTimerEnabled(false);
	}
}

void UTDSCharacterStatsComponent::SetStaminaRecoveryTimerEnabled(const bool Enabled)
{
	if(!GetWorld() || Enabled == GetWorld()->GetTimerManager().IsTimerActive(StaminaRecoveryTimerHandle)) return;

	Enabled
		? GetWorld()->GetTimerManager().SetTimer(StaminaRecoveryTimerHandle, this, &ThisClass::StaminaRecoveryTick, StaminaRecoveryRate, true,
		                                         StaimnaRecoveryDelay)
		: GetWorld()->GetTimerManager().ClearTimer(StaminaRecoveryTimerHandle);
}

void UTDSCharacterStatsComponent::SetStaminaUseTimerEnabled(const bool Enabled)
{
	if(!GetWorld() || Enabled == GetWorld()->GetTimerManager().IsTimerActive(StaminaUseTimerHandle)) return;

	Enabled
		? GetWorld()->GetTimerManager().SetTimer(StaminaUseTimerHandle, this, &ThisClass::StaminaUseTick, StaminaUseRate, true)
		: GetWorld()->GetTimerManager().ClearTimer(StaminaUseTimerHandle);
}
