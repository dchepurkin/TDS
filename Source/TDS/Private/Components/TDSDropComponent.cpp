// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSDropComponent.h"

#include "TDSPickupBase.h"

UTDSDropComponent::UTDSDropComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UTDSDropComponent::Drop()
{
	if(!GetWorld() || !GetOwner()) return;

	auto RandomPickupClassIndex = FMath::RandHelper(DropPickupClasses.Num() - 1);
	const auto PickupClass = DropPickupClasses[static_cast<EDropType>(RandomPickupClassIndex)];
	const auto DropType = *DropPickupClasses.FindKey(PickupClass);
	const auto SpawnTransform = FTransform(GetOwner()->GetActorLocation());
	const auto NewPickup = GetWorld()->SpawnActorDeferred<ATDSPickupBase>(PickupClass,
	                                                                      SpawnTransform,
	                                                                      nullptr,
	                                                                      nullptr,
	                                                                      ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);

	
}
