// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

UTDSHealthComponent::UTDSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	SetHealth(MaxHealth);

	if(GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &ThisClass::TakeDamage);
	}
}

void UTDSHealthComponent::SetHealth(const float NewHealth)
{
	CurrentHealth = FMath::Clamp(NewHealth, 0.f, MaxHealth);
	OnChangeHealth.Broadcast(CurrentHealth, CurrentHealth / MaxHealth);
}

void UTDSHealthComponent::AddHealth(const float HeathToAdd)
{
	SetHealth(CurrentHealth + HeathToAdd);
	OnAddHealth.Broadcast(HeathToAdd);
}

void UTDSHealthComponent::UseHealth(const float HealthToUse)
{
	SetHealth(CurrentHealth - HealthToUse);
	if(IsDead())
	{
		OnDeath.Broadcast();
	}
}

void UTDSHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(IsDead() || FMath::IsNearlyZero(Damage)) return;
	Damage *= DamageCoef;
	OnTakeDamage.Broadcast(Damage);
	UseHealth(Damage);
}
