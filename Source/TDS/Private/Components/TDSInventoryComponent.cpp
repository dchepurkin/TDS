// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSInventoryComponent.h"

#include "TDSGameInstance.h"
#include "TDSWeaponComponent.h"
#include "TDSWeaponPickup.h"
#include "Weapon/TDSWeaponBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogInventoryComponent, All, All);

UTDSInventoryComponent::UTDSInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	InitInventory();
}

void UTDSInventoryComponent::InitInventory()
{
	if(!WeaponSlots.IsValidIndex(0) && !GetOwner()) return;

	const auto WeaponComponent = GetOwner()->FindComponentByClass<UTDSWeaponComponent>();
	if(!WeaponComponent) return;

	for(auto& WeaponSlot : WeaponSlots)
	{
		FWeaponInfo WeaponInfo;
		if(!GetWeaponInfo(WeaponSlot.WeaponTableName, WeaponInfo)) continue;

		WeaponSlot.Weapon = WeaponComponent->CreateWeapon(WeaponInfo.WeaponClass, WeaponInfo.MaxRound);
	}
	WeaponComponent->OnUseRounds.AddUObject(this, &ThisClass::ChangeRounds);
	WeaponComponent->OnReloadEnd.AddUObject(this, &ThisClass::UseClip);
	SetWeapon(CurrentWeaponIndex);
}

bool UTDSInventoryComponent::AddWeapon(const FName& WeaponTableName, const int32& Rounds)
{
	if(IsHaveTakenWeapon(WeaponTableName)) return false;

	int32 EmptySlotIndex;
	if(!GetEmptySlot(EmptySlotIndex)) return false;

	const auto WeaponComponent = GetOwner()->FindComponentByClass<UTDSWeaponComponent>();
	if(!WeaponComponent) return false;

	FWeaponInfo WeaponInfo;
	if(!GetWeaponInfo(WeaponTableName, WeaponInfo)) return false;

	WeaponSlots[EmptySlotIndex].WeaponTableName = WeaponTableName;
	WeaponSlots[EmptySlotIndex].Weapon = WeaponComponent->CreateWeapon(WeaponInfo.WeaponClass, Rounds);
	if(!WeaponSlots[EmptySlotIndex].Weapon) return false;

	OnChangeWeaponSlot.Broadcast(EmptySlotIndex);
	ChangeRounds(WeaponTableName, Rounds);
	return true;
}

void UTDSInventoryComponent::ChangeRounds(const FName& WeaponTableName, const int32 Rounds)
{
	OnChangeRounds.Broadcast(GetWeaponSlotIndexByName(WeaponTableName), Rounds);
}

void UTDSInventoryComponent::UseClip(const FName& WeaponTableName)
{
	const auto FindedSlot = AmmoSlots.FindByPredicate([&](const FAmmoSlotInfo& Slot)
	{
		return WeaponTableName == Slot.WeaponTableName;
	});
	if(!FindedSlot || FindedSlot->bInfinity) return;

	OnChangeClips.Broadcast(WeaponTableName, --FindedSlot->ClipsAmount);
}

UTexture2D* UTDSInventoryComponent::GetWeaponIcon(const FName& WeaponTableName) const
{
	FWeaponInfo WeaponInfo;
	GetWeaponInfo(WeaponTableName, WeaponInfo);

	return WeaponInfo.WeaponIcon ? WeaponInfo.WeaponIcon : DefaultSlotIcon;
}

void UTDSInventoryComponent::GetAmmoInfo(const FName& WeaponTableName, int32& Rounds) const
{
	FWeaponInfo WeaponInfo;
	Rounds = GetWeaponInfo(WeaponTableName, WeaponInfo) ? WeaponInfo.MaxRound : 0;
}

void UTDSInventoryComponent::SetWeapon(const int32 WeaponIndex)
{
	if(!WeaponSlots.IsValidIndex(WeaponIndex)) return;

	const auto WeaponComponent = GetOwner()->FindComponentByClass<UTDSWeaponComponent>();
	if(!WeaponComponent) return;

	if(WeaponComponent->SetCurrentWeapon(WeaponSlots[WeaponIndex].Weapon))
	{
		CurrentWeaponIndex = WeaponIndex;
		OnChangeWeapon.Broadcast(CurrentWeaponIndex);
	}
}

bool UTDSInventoryComponent::HaveClips(const FName& WeaponTableName) const
{
	return GetClips(WeaponTableName) > 0;
}

bool UTDSInventoryComponent::AddAmmo(const FName& WeaponTableName, const int32 Clips)
{
	const auto FindedSlot = AmmoSlots.FindByPredicate([&](const FAmmoSlotInfo& Slot)
	{
		return WeaponTableName == Slot.WeaponTableName;
	});
	if(!FindedSlot) return false;

	FindedSlot->ClipsAmount += Clips;
	OnChangeClips.Broadcast(WeaponTableName, FindedSlot->ClipsAmount);
	TryToReload(WeaponTableName);

	return true;
}

void UTDSInventoryComponent::TryToReload(const FName& WeaponTableName)
{
	if(!GetOwner()) return;

	const auto WeaponComponent = GetOwner()->FindComponentByClass<UTDSWeaponComponent>();
	if(!WeaponComponent) return;

	const auto Weapon = WeaponSlots[GetWeaponSlotIndexByName(WeaponTableName)].Weapon;
	WeaponComponent->TryToReload(Weapon);
}

void UTDSInventoryComponent::DropWeapon()
{
	if(!DropWeaponPickupClass || !GetWorld() || !GetOwner() || CurrentWeaponIndex == 0 || !WeaponSlots[CurrentWeaponIndex].Weapon) return;

	constexpr float SpawnDistance = 100.f;
	const auto SpawnLocation = GetOwner()->GetActorLocation() + GetOwner()->GetActorForwardVector() * SpawnDistance;
	const auto SpawnTransform = FTransform(SpawnLocation);

	const auto WeaponPickup = GetWorld()->SpawnActorDeferred<ATDSWeaponPickup>(DropWeaponPickupClass,
	                                                                           SpawnTransform,
	                                                                           nullptr,
	                                                                           nullptr,
	                                                                           ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);

	if(!WeaponPickup || !WeaponSlots.IsValidIndex(CurrentWeaponIndex)) return;

	WeaponPickup->SetWeaponName(WeaponSlots[CurrentWeaponIndex].WeaponTableName);
	WeaponPickup->SetRounds(WeaponSlots[CurrentWeaponIndex].Weapon->GetRoundCount());
	WeaponPickup->FinishSpawning(SpawnTransform);

	WeaponSlots[CurrentWeaponIndex].WeaponTableName = NAME_None;
	WeaponSlots[CurrentWeaponIndex].Weapon->Destroy();
	WeaponSlots[CurrentWeaponIndex].Weapon = nullptr;

	OnChangeWeaponSlot.Broadcast(CurrentWeaponIndex);
	SetWeapon(0);
}

bool UTDSInventoryComponent::GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const
{
	const auto GameInstance = GetWorld()->GetGameInstance<UTDSGameInstance>();
	if(!GameInstance) return false;

	return GameInstance->GetWeaponInfo(WeaponTableName, WeaponInfo);
}

int32 UTDSInventoryComponent::GetClips(const FName& WeaponTableName) const
{
	const auto FindedSlot = AmmoSlots.FindByPredicate([&](const FAmmoSlotInfo& Slot)
	{
		return WeaponTableName == Slot.WeaponTableName;
	});
	if(!FindedSlot) return 0;

	return FindedSlot->ClipsAmount;
}

bool UTDSInventoryComponent::GetEmptySlot(int32& EmptySlotIndex)
{
	for(int32 i = 0; i < WeaponSlots.Num(); ++i)
	{
		if(WeaponSlots[i].WeaponTableName.IsNone())
		{
			EmptySlotIndex = i;
			return true;
		}
	}

	return false;
}

bool UTDSInventoryComponent::IsHaveTakenWeapon(const FName& WeaponTableName)
{
	const auto FindedSlot = WeaponSlots.FindByPredicate([&](const FWeaponSlotInfo& Slot)
	{
		return Slot.WeaponTableName == WeaponTableName;
	});

	return FindedSlot ? true : false;
}

int32 UTDSInventoryComponent::GetWeaponSlotIndexByName(const FName& WeaponTableName)
{
	for(int32 i = 0; i < WeaponSlots.Num(); ++i)
	{
		if(WeaponSlots[i].WeaponTableName == WeaponTableName)
		{
			return i;
		}
	}

	return 0;
}
