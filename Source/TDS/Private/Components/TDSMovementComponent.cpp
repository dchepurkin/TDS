// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSMovement, All, All);

void UTDSMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDSMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	bOrientRotationToMovement = IsSprint();
}

bool UTDSMovementComponent::IsSprint() const
{
	return !IsAimState && IsSprintState && IsMoving();
}

void UTDSMovementComponent::SetMovementState(const EMovementState NewMovementState)
{
	CharacterMovementState = NewMovementState;
	OnChangeMovementState.Broadcast(CharacterMovementState);
}

void UTDSMovementComponent::OnAimAction(const bool IsAimEnabled)
{
	IsAimState = IsAimEnabled;

	if(IsAimState)
	{
		PrevMovementState = CharacterMovementState;
		SetMovementState(EMovementState::MS_AimState);
	}
	else
	{
		SetMovementState(PrevMovementState);
	}
}

void UTDSMovementComponent::OnWalkAction(const bool IsWalkEnabled)
{
	if(IsSprintState) return;

	IsWalkState = IsWalkEnabled;

	IsAimState
		? PrevMovementState = IsWalkState ? EMovementState::MS_WalkState : EMovementState::MS_RunState
		: SetMovementState(IsWalkState ? EMovementState::MS_WalkState : EMovementState::MS_RunState);
}

void UTDSMovementComponent::SetSprint(const bool IsSprintEnabled)
{
	if(IsWalkState) return;

	IsSprintState = IsSprintEnabled;

	IsAimState
		? PrevMovementState = IsSprintState ? EMovementState::MS_SprintState : EMovementState::MS_RunState
		: SetMovementState(IsSprintState ? EMovementState::MS_SprintState : EMovementState::MS_RunState);
}

float UTDSMovementComponent::GetMaxSpeed() const
{
	return MovementSpeedInfo.Speed[CharacterMovementState];
}
