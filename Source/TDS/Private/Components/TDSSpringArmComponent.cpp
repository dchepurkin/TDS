// Created by Dmitriy Chepurkin, All Right Reserved

#include "TDSSpringArmComponent.h"

void UTDSSpringArmComponent::BeginPlay()
{
	Super::BeginPlay();

	CameraHeightChangeTo = TargetArmLength;
}

void UTDSSpringArmComponent::ChangeCameraHeight(const float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;
	CameraHeightChangeTo = GetNewArmLength(AxisValue);

	if(!bSlideWithSmooth) TargetArmLength = CameraHeightChangeTo;
}

void UTDSSpringArmComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(bSlideWithSmooth) ArmLengthSlide(DeltaTime);
}

void UTDSSpringArmComponent::ArmLengthSlide(const float DeltaTime)
{
	if(FMath::IsNearlyEqual(TargetArmLength, CameraHeightChangeTo)) return;

	const auto NewArmLength = FMath::FInterpConstantTo(TargetArmLength, CameraHeightChangeTo, DeltaTime, ArmLengthSlideSpeed);
	TargetArmLength = NewArmLength;
}

float UTDSSpringArmComponent::GetNewArmLength(const float StepCoef) const
{
	const auto LengthStep = ArmLengthChangeStep * StepCoef;
	return FMath::Clamp(TargetArmLength + LengthStep, MinArmLength, MaxArmLength);
}
