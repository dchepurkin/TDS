// Created by Dmitriy Chepurkin, All Right Reserved

#include "Components/TDSWeaponComponent.h"

#include "TDSGameInstance.h"
#include "Engine/DataTable.h"
#include "Weapon/TDSWeaponBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All, All);

UTDSWeaponComponent::UTDSWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UTDSWeaponComponent::BeginPlay()
{
	Super::BeginPlay();
}

ATDSWeaponBase* UTDSWeaponComponent::CreateWeapon(const TSubclassOf<ATDSWeaponBase> WeaponClass, const int32& Rounds) const
{
	if(!WeaponClass) return nullptr;

	const auto World = GetWorld();
	if(!World) return nullptr;

	const auto NewWeapon = World->SpawnActorDeferred<ATDSWeaponBase>(WeaponClass, FTransform::Identity, GetOwner());
	if(!NewWeapon) return nullptr;

	NewWeapon->SetOwner(GetOwner());
	NewWeapon->SetRoundAmount(Rounds);
	NewWeapon->FinishSpawning(FTransform::Identity);
	return NewWeapon;
}

bool UTDSWeaponComponent::SetCurrentWeapon(ATDSWeaponBase* Weapon)
{
	if(!Weapon || Weapon == CurrentWeapon) return false;
	if(!CanChangeWeapon()) return false;

	if(CurrentWeapon)
	{
		CurrentWeapon->SetVisibility(false);
		CurrentWeapon->StopFire();
	}

	CurrentWeapon = Weapon;

	AttachToComponent = GetOwner()->FindComponentByClass<UMeshComponent>();
	CurrentWeapon->AttachToComponent(AttachToComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale,
	                                 GetWeaponSocketName(CurrentWeapon->GetWeaponTableName()));

	CurrentWeapon->SetVisibility(true);
	TryToReload(CurrentWeapon);
	return true;
}

void UTDSWeaponComponent::GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const
{
	const auto GameInstance = GetWorld()->GetGameInstance<UTDSGameInstance>();
	if(!GameInstance) return;

	GameInstance->GetWeaponInfo(WeaponTableName, WeaponInfo);
}

bool UTDSWeaponComponent::GetProjectileInfo(const FName& ProjectileTableName, FProjectileInfo& ProjectileInfo) const
{
	if(!ProjectileDataTable) return false;

	const auto FindedProjectileInfo = ProjectileDataTable->FindRow<FProjectileInfo>(ProjectileTableName, "");
	if(!FindedProjectileInfo) return false;

	ProjectileInfo = *FindedProjectileInfo;
	return true;
}

FName UTDSWeaponComponent::GetWeaponSocketName(const FName& WeaponTableName) const
{
	FWeaponInfo WeaponInfo;
	GetWeaponInfo(WeaponTableName, WeaponInfo);

	return WeaponInfo.AttachToSocketName;
}

bool UTDSWeaponComponent::CanChangeWeapon() const
{
	return !CurrentWeapon || !CurrentWeapon->IsReloading();
}

void UTDSWeaponComponent::StartFire() const
{
	if(!CurrentWeapon) return;

	CurrentWeapon->StartFire();
}

void UTDSWeaponComponent::StopFire() const
{
	if(!CurrentWeapon) return;

	CurrentWeapon->StopFire();
}

void UTDSWeaponComponent::ReloadWeapon() const
{
	if(!CurrentWeapon) return;

	CurrentWeapon->StartReload();
}

bool UTDSWeaponComponent::IsReload() const
{
	return CurrentWeapon && CurrentWeapon->IsReloading();
}

void UTDSWeaponComponent::TryToReload(ATDSWeaponBase* Weapon) const
{
	if(CurrentWeapon == Weapon && CurrentWeapon->IsClipEmpty())
	{
		ReloadWeapon();
	}
}
