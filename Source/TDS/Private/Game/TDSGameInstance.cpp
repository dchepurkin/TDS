// Created by Dmitriy Chepurkin, All Right Reserved

#include "Game/TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const
{
	if(WeaponTableName.IsNone() || !WeaponDataTable) return false;

	const auto FindedWeaponInfo = WeaponDataTable->FindRow<FWeaponInfo>(WeaponTableName, "");
	if(!FindedWeaponInfo) return false;

	WeaponInfo = *FindedWeaponInfo;
	return true;
}

void UTDSGameInstance::GetProjectileInfo(const FName& ProjectileTableName, FProjectileInfo& ProjectileInfo) const
{
	if(!ProjectileDataTable) return;

	const auto FindedProjectileInfo = ProjectileDataTable->FindRow<FProjectileInfo>(ProjectileTableName, "");
	if(!FindedProjectileInfo) return;

	ProjectileInfo = *FindedProjectileInfo;
}
