// Created by Dmitriy Chepurkin, All Right Reserved

#include "TDSGameMode.h"
#include "TDSPlayerController.h"
#include "TDSCharacter.h"

ATDSGameMode::ATDSGameMode()
{
	PlayerControllerClass = ATDSPlayerController::StaticClass();
	DefaultPawnClass = ATDSCharacter::StaticClass();
}
