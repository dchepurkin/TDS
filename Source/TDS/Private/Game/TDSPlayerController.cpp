// Created by Dmitriy Chepurkin, All Right Reserved

#include "TDSPlayerController.h"
#include "TDSCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSPlayerController, All, All);

ATDSPlayerController::ATDSPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if(InputComponent)
	{
		InputComponent->BindAction("Pause", IE_Pressed, this, &ThisClass::TogglePause).bExecuteWhenPaused = true;
	}
}

void ATDSPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeGameOnly());
	SetShowMouseCursor(false);
}

void ATDSPlayerController::TogglePause()
{
	UE_LOG(LogTDSPlayerController, Display, TEXT("PAUSE"));
	SetPause(!IsPaused());
	SetShowMouseCursor(IsPaused());
	CurrentMouseCursor = IsPaused() ? EMouseCursor::Default : EMouseCursor::Crosshairs;

	IsPaused()
		? SetInputMode(FInputModeGameAndUI())
		: SetInputMode(FInputModeGameOnly());

	OnPauseGame.Broadcast(IsPaused());
}
