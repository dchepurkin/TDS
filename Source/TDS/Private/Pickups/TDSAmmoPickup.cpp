// Created by Dmitriy Chepurkin, All Right Reserved

#include "Pickups/TDSAmmoPickup.h"

#include "TDSInventoryComponent.h"

void ATDSAmmoPickup::TryToTakePickup(AActor* Player)
{
	if(!Player) return;

	const auto Inventory = Player->FindComponentByClass<UTDSInventoryComponent>();
	if(!Inventory) return;

	if(Inventory->AddAmmo(WeaponTableName, Clips))
	{
		Super::TryToTakePickup(Player);
	}
}
