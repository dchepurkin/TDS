// Created by Dmitriy Chepurkin, All Right Reserved

#include "Pickups/TDSHealthPickup.h"

#include "TDSCharacterStatsComponent.h"

void ATDSHealthPickup::TryToTakePickup(AActor* Player)
{
	if(!Player) return;

	const auto StatsComponent = Player->FindComponentByClass<UTDSCharacterStatsComponent>();
	if(!StatsComponent || StatsComponent->IsFullHealth() || StatsComponent->IsDead()) return;

	StatsComponent->AddHealth(Health);
	Super::TryToTakePickup(Player);
}
