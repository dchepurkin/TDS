// Created by Dmitriy Chepurkin, All Right Reserved

#include "Pickups/TDSPickupBase.h"

#include "TDSCharacter.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

ATDSPickupBase::ATDSPickupBase()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>("Collision");
	Collision->SetCollisionResponseToAllChannels(ECR_Ignore);
	Collision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SetRootComponent(Collision);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	StaticMesh->SetupAttachment(Collision);

	Particle = CreateDefaultSubobject<UParticleSystemComponent>("Particle");
	Particle->SetupAttachment(Collision);
}

void ATDSPickupBase::BeginPlay()
{
	Super::BeginPlay();

	if(Collision)
	{
		Collision->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnCollisionBeginOverlap);
	}
}

void ATDSPickupBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RotationTick(DeltaTime);
}

void ATDSPickupBase::RotationTick(const float DeltaSeconds) const
{
	const auto RotationYawOffset = RotationRate * DeltaSeconds;
	const auto RotationOffset = FRotator(0.f, RotationYawOffset, 0.f);
	StaticMesh->AddRelativeRotation(RotationOffset);
}

void ATDSPickupBase::OnCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                             AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp,
                                             int32 OtherBodyIndex,
                                             bool bFromSweep,
                                             const FHitResult& SweepResult)
{
	const auto Player = Cast<ATDSCharacter>(OtherActor);
	if(!Player) return;

	TryToTakePickup(OtherActor);
}

void ATDSPickupBase::TryToTakePickup(AActor* Player)
{
	UGameplayStatics::PlaySound2D(GetWorld(), OnTakeSound);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OnTakeFX, GetActorLocation());
	Destroy();
}
