// Created by Dmitriy Chepurkin, All Right Reserved

#include "Pickups/TDSWeaponPickup.h"

#include "TDSGameInstance.h"
#include "TDSInventoryComponent.h"

void ATDSWeaponPickup::BeginPlay()
{
	Super::BeginPlay();

	if(StaticMesh)
	{
		StaticMesh->SetStaticMesh(GetWeaponMesh());
		StaticMesh->SetWorldScale3D(FVector(0.75, 0.75, 0.75));
	}
}

UStaticMesh* ATDSWeaponPickup::GetWeaponMesh() const
{
	if(!GetWorld()) return nullptr;

	const auto GameInstance = GetWorld()->GetGameInstance<UTDSGameInstance>();
	if(!GameInstance) return nullptr;

	FWeaponInfo WeaponInfo;
	GameInstance->GetWeaponInfo(WeaponTableName, WeaponInfo);

	return WeaponInfo.WeaponMesh;
}

void ATDSWeaponPickup::TryToTakePickup(AActor* Player)
{
	if(!Player) return;

	const auto Inventory = Player->FindComponentByClass<UTDSInventoryComponent>();
	if(!Inventory) return;

	if(Inventory->AddWeapon(WeaponTableName, Rounds))
	{
		Super::TryToTakePickup(Player);
	}
}
