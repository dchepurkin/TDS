// Created by Dmitriy Chepurkin, All Right Reserved

#include "Weapon/TDSAutoShotWeapon.h"

void ATDSAutoShotWeapon::StartFire()
{
	if(CanFire())
	{
		AutoShot();		
		if(!IsClipEmpty()) SetAutoShotTimerEnabled(true);
	}
	else if(IsAmmoEmpty())
	{
		PlaySound(WeaponInfo.EmptyAmmoSound);
	}
}

void ATDSAutoShotWeapon::StopFire()
{
	Super::StopFire();
	SetAutoShotTimerEnabled(false);
}

void ATDSAutoShotWeapon::AutoShot()
{
	MakeShoot();
	PlayMuzzleFX();
	PlaySound(WeaponInfo.FireSound);
	StartCooldown();
}

void ATDSAutoShotWeapon::SetAutoShotTimerEnabled(const bool Enabled)
{
	if(Enabled && GetWorldTimerManager().IsTimerActive(AutoShotTimerHandle)) return;

	Enabled
		? GetWorldTimerManager().SetTimer(AutoShotTimerHandle, this, &ThisClass::AutoShot, WeaponInfo.RateOfFire, true)
		: GetWorldTimerManager().ClearTimer(AutoShotTimerHandle);
}
