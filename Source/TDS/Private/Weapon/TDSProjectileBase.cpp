// Created by Dmitriy Chepurkin, All Right Reserved

#include "Weapon/TDSProjectileBase.h"

#include "DrawDebugHelpers.h"
#include "TDSBuffBase.h"
#include "TDSBuffComponent.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

DEFINE_LOG_CATEGORY_STATIC(LogProjectile, All, All);

ATDSProjectileBase::ATDSProjectileBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	ProjectileCollision = CreateDefaultSubobject<USphereComponent>("ProjectileCollision");
	ProjectileCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	ProjectileCollision->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	ProjectileCollision->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	ProjectileCollision->bReturnMaterialOnMove = true;
	SetRootComponent(ProjectileCollision);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetupAttachment(ProjectileCollision);

	FX = CreateDefaultSubobject<UParticleSystemComponent>("FX");
	FX->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FX->SetupAttachment(ProjectileCollision);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
}

void ATDSProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	if(ProjectileCollision)
	{
		ProjectileCollision->OnComponentHit.AddDynamic(this, &ThisClass::OnHit);
		ProjectileCollision->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBeginOverlap);
	}

	if(StaticMesh)
	{
		if(ProjectileInfo.Mesh) StaticMesh->SetStaticMesh(ProjectileInfo.Mesh);
	}

	if(FX)
	{
		if(ProjectileInfo.TrailFX) FX->SetTemplate(ProjectileInfo.TrailFX);
	}
}

void ATDSProjectileBase::OnHit(UPrimitiveComponent* HitComponent,
                               AActor* OtherActor,
                               UPrimitiveComponent* OtherComp,
                               FVector NormalImpulse,
                               const FHitResult& Hit)
{
	if(Hit.bBlockingHit)
	{
		PlayImpactSound(Hit);
		PlayImpactFX(Hit);
		SpawnImpactDecal(Hit);
		if(ProjectileInfo.bRadialDamage) MakeRadialDamage();
	}
	if(!ProjectileInfo.bCanBounce)
	{
		Destroy();
	}
}

void ATDSProjectileBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                        AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp,
                                        int32 OtherBodyIndex,
                                        bool bFromSweep,
                                        const FHitResult& SweepResult)
{
	if(OtherActor == GetOwner()) return;
	
	ProjectileInfo.bRadialDamage
		? MakeRadialDamage()
		: MakePointDamage(OtherActor);

	PlayImpactSound(SweepResult);
	PlayImpactFX(SweepResult);

	Destroy();
}

void ATDSProjectileBase::MakeRadialDamage()
{
	UGameplayStatics::ApplyRadialDamage(this,
	                                    ProjectileInfo.Damage,
	                                    GetActorLocation(),
	                                    ProjectileInfo.DamageRadius,
	                                    nullptr,
	                                    {GetOwner()},
	                                    GetOwner());

	TArray<AActor*> DamagedActors;
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
	                                          GetActorLocation(),
	                                          ProjectileInfo.DamageRadius,
	                                          {},
	                                          AActor::StaticClass(),
	                                          {GetOwner()},
	                                          DamagedActors);
	TryToApplyBuff(DamagedActors);

	if(ProjectileInfo.bDebug && GetWorld())
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileInfo.DamageRadius / 2, 16, FColor::Blue, false, 2.0f, 0, 1.5f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileInfo.DamageRadius, 16, FColor::Red, false, 2.0f, 0, 1.5f);
	}
}

void ATDSProjectileBase::MakePointDamage(AActor* DamagedActor)
{
	UGameplayStatics::ApplyDamage(DamagedActor, ProjectileInfo.Damage, nullptr, GetOwner(), nullptr);
	TryToApplyBuff(DamagedActor);
}

void ATDSProjectileBase::TryToApplyBuff(TArray<AActor*>& Actors)
{
	for(const auto& Actor : Actors)
	{
		TryToApplyBuff(Actor);
	}
}

void ATDSProjectileBase::TryToApplyBuff(AActor* Actor)
{
	if(!Actor || !ProjectileInfo.Buff) return;

	const auto BuffComponent = Actor->FindComponentByClass<UTDSBuffComponent>();
	if(!BuffComponent) return;

	BuffComponent->TryToAddBuff(ProjectileInfo.Buff, GetOwner());
}

void ATDSProjectileBase::PlayImpactSound(const FHitResult& HitResult) const
{
	if(!ProjectileInfo.ImpactSounds.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto Sound = ProjectileInfo.ImpactSounds.Contains(HitResult.PhysMaterial->SurfaceType)
		                   ? ProjectileInfo.ImpactSounds[HitResult.PhysMaterial->SurfaceType]
		                   : ProjectileInfo.ImpactSounds[SurfaceType_Default];

	UGameplayStatics::PlaySoundAtLocation(this, Sound, HitResult.ImpactPoint);
}

void ATDSProjectileBase::PlayImpactFX(const FHitResult& HitResult) const
{
	if(!ProjectileInfo.ImpactFXs.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto ParticleSystem = ProjectileInfo.ImpactFXs.Contains(HitResult.PhysMaterial->SurfaceType)
		                            ? ProjectileInfo.ImpactFXs[HitResult.PhysMaterial->SurfaceType]
		                            : ProjectileInfo.ImpactFXs[SurfaceType_Default];

	UGameplayStatics::SpawnEmitterAtLocation(this, ParticleSystem, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation());
}

void ATDSProjectileBase::SpawnImpactDecal(const FHitResult& HitResult) const
{
	if(!ProjectileInfo.ImpactDecals.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto DecalMaterial = ProjectileInfo.ImpactDecals.Contains(HitResult.PhysMaterial->SurfaceType)
		                           ? ProjectileInfo.ImpactDecals[HitResult.PhysMaterial->SurfaceType]
		                           : ProjectileInfo.ImpactDecals[SurfaceType_Default];

	const auto Decal = UGameplayStatics::SpawnDecalAtLocation(this,
	                                                          DecalMaterial,
	                                                          ProjectileInfo.ImpactDecalSize,
	                                                          HitResult.ImpactPoint,
	                                                          HitResult.ImpactPoint.Rotation(),
	                                                          ProjectileInfo.ImpactDecalLifeTime);
	Decal->SetFadeOut(ProjectileInfo.ImpactDecalLifeTime, ProjectileInfo.ImpactDecalFadeoutTime);
}

void ATDSProjectileBase::InitProjectile(const bool bUseImportProjectileInfo, const FProjectileInfo& NewProjectileInfo, AActor* IgnoredActor)
{
	if(bUseImportProjectileInfo)
	{
		ProjectileInfo = NewProjectileInfo;
	}

	SetLifeSpan(ProjectileInfo.LifeTime);

	if(ProjectileMovement)
	{
		ProjectileMovement->InitialSpeed = ProjectileInfo.InitSpeed;
		ProjectileMovement->bShouldBounce = ProjectileInfo.bCanBounce;
	}

	if(ProjectileCollision)
	{
		ProjectileCollision->IgnoreActorWhenMoving(IgnoredActor, true);
	}
}
