// Created by Dmitriy Chepurkin, All Right Reserved

#include "Weapon/TDSSleeve.h"

#include "GameFramework/ProjectileMovementComponent.h"

ATDSSleeve::ATDSSleeve()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	StaticMesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	SetRootComponent(StaticMesh);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->bBounceAngleAffectsFriction = true;
	ProjectileMovement->Bounciness = 0.15f;
	ProjectileMovement->InitialSpeed = 200.f;
	ProjectileMovement->MaxSpeed = 200.f;
}

void ATDSSleeve::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(LifeTime);
}

void ATDSSleeve::SetStaticMesh(UStaticMesh* NewMesh) const
{
	if(!StaticMesh || !NewMesh) return;

	StaticMesh->SetStaticMesh(NewMesh);
}
