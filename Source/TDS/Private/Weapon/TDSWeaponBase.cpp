// Created by Dmitriy Chepurkin, All Right Reserved

#include "Weapon/TDSWeaponBase.h"

#include "DrawDebugHelpers.h"
#include "TDSBuffBase.h"
#include "TDSBuffComponent.h"
#include "TDSInventoryComponent.h"
#include "TDSMovementComponent.h"
#include "TDSWeaponComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Weapon/TDSProjectileBase.h"
#include "Weapon/TDSSleeve.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponBase, All, All);

ATDSWeaponBase::ATDSWeaponBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetVisibility(false);
	SetRootComponent(StaticMesh);

	Muzzle = CreateDefaultSubobject<UArrowComponent>("Muzzle");
	Muzzle->SetupAttachment(StaticMesh);

	SleeveDropPoint = CreateDefaultSubobject<UArrowComponent>("SleeveDropPoint");
	SleeveDropPoint->SetArrowColor(FLinearColor::Yellow);
	SleeveDropPoint->SetupAttachment(StaticMesh);

	MagazineDropPoint = CreateDefaultSubobject<UArrowComponent>("MagazineDropPoint");
	MagazineDropPoint->SetArrowColor(FLinearColor::Blue);
	MagazineDropPoint->ArrowSize = 0.5;
	MagazineDropPoint->SetupAttachment(StaticMesh);
}

void ATDSWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	WeaponComponent = GetOwner()->FindComponentByClass<UTDSWeaponComponent>();
	if(WeaponComponent)
	{
		WeaponComponent->GetWeaponInfo(WeaponTableName, WeaponInfo);
	}
}

void ATDSWeaponBase::StartFire()
{
	if(CanFire())
	{
		MakeShoot();
		PlayMuzzleFX();
		PlaySound(WeaponInfo.FireSound);
	}
	else if(IsAmmoEmpty())
	{
		PlaySound(WeaponInfo.EmptyAmmoSound);
	}
}

bool ATDSWeaponBase::CanFire() const
{
	return !bInCooldown && !IsClipEmpty() && !bIsReloading;
}

void ATDSWeaponBase::MakeShoot()
{
	const auto RoundPerShoot = GetRoundPerShoot();
	for(int32 i = 0; i < RoundPerShoot; ++i)
	{
		WeaponInfo.bIsProjectileWeapon ? CreateProjectile() : MakeTraceShot();
	}
	UseRound(GetRoundPerShoot());
	StartCooldown();
	if(WeaponComponent)
	{
		WeaponComponent->OnMakeShoot.Broadcast(WeaponInfo.FireAnimations);
	}
}

void ATDSWeaponBase::CreateProjectile()
{
	if(!GetWorld()) return;

	FTransform SpawnTransform;
	GetProjectileTransform(SpawnTransform);

	const auto NewProjectile = GetWorld()->SpawnActorDeferred<ATDSProjectileBase>(WeaponInfo.ProjectileClass,
	                                                                              SpawnTransform,
	                                                                              nullptr,
	                                                                              nullptr,
	                                                                              ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if(!NewProjectile) return;

	FProjectileInfo ProjectileInfo;
	const auto bUseImportProjectileInfo = WeaponComponent && WeaponComponent->GetProjectileInfo(NewProjectile->GetTableName(), ProjectileInfo);

	NewProjectile->InitProjectile(bUseImportProjectileInfo, ProjectileInfo, GetOwner());
	NewProjectile->SetOwner(GetOwner());
	NewProjectile->FinishSpawning(SpawnTransform);
}

void ATDSWeaponBase::MakeTraceShot()
{
	if(!GetWorld() || !Muzzle) return;

	const auto PC = GetWorld()->GetFirstPlayerController();
	if(!PC) return;

	FTransform TraceStart;
	GetMuzzleTransform(TraceStart);
	FVector TraceEnd = Muzzle->GetForwardVector();

	FHitResult HitResult;
	PC->GetHitResultUnderCursorByChannel(TraceTypeQuery3, false, HitResult);

	constexpr float MinCursorDistance = 100.f;
	if(HitResult.bBlockingHit && FVector::Dist2D(GetActorLocation(), HitResult.Location) >= MinCursorDistance)
	{
		TraceEnd = FMath::VRandCone(HitResult.Location - TraceStart.GetLocation(), GetConeHalfAngle());
	}

	TraceEnd = TraceEnd * WeaponInfo.TraceDistance + TraceStart.GetLocation();
	TraceEnd.Z = TraceStart.GetLocation().Z;

	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(this);
	CollisionQueryParams.AddIgnoredActor(GetOwner());
	CollisionQueryParams.bReturnPhysicalMaterial = true;
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart.GetLocation(), TraceEnd, ECC_Visibility, CollisionQueryParams);

	if(HitResult.bBlockingHit)
	{
		HitResult.Actor->TakeDamage(WeaponInfo.Damage, FDamageEvent(), nullptr, GetOwner());
		TryToApplyBuff(HitResult.GetActor());
		PlayImpactSound(HitResult);
		PlayImpactFX(HitResult);
		SpawnImpactDecal(HitResult);
	}
	PlayTrailFX(HitResult.bBlockingHit ? HitResult.ImpactPoint : HitResult.TraceEnd);

	if(WeaponInfo.bDebug)
	{
		DrawDebugLine(GetWorld(), TraceStart.GetLocation(), TraceEnd, FColor::Red, false, 1.f, 0, 1.5f);
	}
}

void ATDSWeaponBase::TryToApplyBuff(AActor* Actor)
{
	if(!Actor || !WeaponInfo.Buff) return;

	const auto BuffComponent = Actor->FindComponentByClass<UTDSBuffComponent>();
	if(!BuffComponent) return;

	BuffComponent->TryToAddBuff(WeaponInfo.Buff, GetOwner());
}

void ATDSWeaponBase::GetProjectileTransform(FTransform& Transform)
{
	if(!GetWorld()) return;

	GetMuzzleTransform(Transform);

	const auto PC = GetWorld()->GetFirstPlayerController();
	if(!PC) return;

	FHitResult HitResult;
	PC->GetHitResultUnderCursorByChannel(TraceTypeQuery3, false, HitResult);

	if(!HitResult.bBlockingHit) return;

	constexpr float MinCursorDistance = 100.f;
	if(FVector::Dist2D(GetActorLocation(), HitResult.Location) < MinCursorDistance) return;

	auto Direction = FMath::VRandCone(HitResult.Location - Transform.GetLocation(), GetConeHalfAngle());
	Direction *= WeaponInfo.TraceDistance;
	Direction.Z = Transform.GetLocation().Z;
	const FMatrix RotationMatrix{Direction, FVector(0.f, 1.f, 0.f), FVector(0.f, 0.f, 1.f), FVector::ZeroVector};
	Transform.SetRotation(RotationMatrix.Rotator().Quaternion());
}

float ATDSWeaponBase::GetConeHalfAngle()
{
	if(!GetOwner()) return 0.f;

	const auto MovementComponent = GetOwner()->FindComponentByClass<UTDSMovementComponent>();
	if(!MovementComponent) return 0.f;

	const auto MovementState = MovementComponent->GetMovementState();
	return FMath::DegreesToRadians(FMath::RandRange(WeaponInfo.Dispersion[MovementState].DispersionMin, WeaponInfo.Dispersion[MovementState].DispersionMax));
}

void ATDSWeaponBase::DropSleeve() const
{
	if(!GetWorld() || !SleeveDropPoint) return;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	const auto NewSleeve = GetWorld()->SpawnActor<ATDSSleeve>(WeaponInfo.DropActorClass,
	                                                          SleeveDropPoint->GetComponentLocation(),
	                                                          UKismetMathLibrary::RandomRotator(),
	                                                          SpawnParameters);

	NewSleeve->SetStaticMesh(WeaponInfo.SleeveMesh);
}

void ATDSWeaponBase::DropMagazine() const
{
	if(!GetWorld() || !MagazineDropPoint) return;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	const auto NewSleeve = GetWorld()->SpawnActor<ATDSSleeve>(WeaponInfo.DropActorClass,
	                                                          MagazineDropPoint->GetComponentLocation(),
	                                                          MagazineDropPoint->GetComponentRotation(),
	                                                          SpawnParameters);

	NewSleeve->SetStaticMesh(WeaponInfo.MagazineMesh);
}

void ATDSWeaponBase::PlaySound(USoundBase* Sound) const
{
	FTransform MuzzleTransform;
	GetMuzzleTransform(MuzzleTransform);

	UGameplayStatics::PlaySoundAtLocation(this, Sound, MuzzleTransform.GetLocation());
}

void ATDSWeaponBase::GetMuzzleTransform(FTransform& Transform) const
{
	Transform = Muzzle ? Muzzle->GetComponentTransform() : FTransform::Identity;
}

void ATDSWeaponBase::StartCooldown()
{
	bInCooldown = true;
	GetWorldTimerManager().SetTimer(CooldownFireTimerHandle, this, &ThisClass::OnFireCooldownEnd, WeaponInfo.RateOfFire);
}

void ATDSWeaponBase::OnFireCooldownEnd()
{
	bInCooldown = false;
}

bool ATDSWeaponBase::IsClipEmpty() const
{
	return RoundCount == 0;
}

bool ATDSWeaponBase::IsAmmoEmpty() const
{
	bool Result = false;

	if(GetOwner())
	{
		const auto Inventory = GetOwner()->FindComponentByClass<UTDSInventoryComponent>();
		if(Inventory)
		{
			Result = !Inventory->HaveClips(WeaponTableName);
		}
	}

	return Result;
}

void ATDSWeaponBase::UseRound(const int32 Count)
{
	SetRoundAmount(RoundCount - Count);

	for(int32 i = 0; i < Count; ++i)
	{
		DropSleeve();
	}
	if(RoundCount == 0)
	{
		StopFire();
		StartReload();
	}
}

int32 ATDSWeaponBase::GetRoundPerShoot() const
{
	return RoundCount >= WeaponInfo.RoundPerShoot ? WeaponInfo.RoundPerShoot : RoundCount;
}

void ATDSWeaponBase::SetVisibility(const bool Visibility) const
{
	if(StaticMesh)
	{
		StaticMesh->SetVisibility(Visibility);
	}
}

void ATDSWeaponBase::StartReload()
{
	if(!CanReload()) return;

	bIsReloading = true;
	GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &ThisClass::OnReloadFinished, WeaponInfo.ReloadTime);
	PlaySound(WeaponInfo.ReloadSound);
	DropMagazine();

	if(WeaponComponent)
	{
		WeaponComponent->OnReloadStart.Broadcast(WeaponInfo.ReloadAnimation, WeaponInfo.ReloadTime);
	}
}

bool ATDSWeaponBase::CanReload() const
{
	return !bIsReloading &&
		RoundCount < WeaponInfo.MaxRound &&
		!IsAmmoEmpty();
}

void ATDSWeaponBase::OnReloadFinished()
{
	bIsReloading = false;
	SetRoundAmount(WeaponInfo.MaxRound);

	if(WeaponComponent)
	{
		WeaponComponent->OnReloadEnd.Broadcast(WeaponTableName);
	}
}

void ATDSWeaponBase::PlayImpactSound(const FHitResult& HitResult) const
{
	if(!WeaponInfo.ImpactSounds.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto Sound = WeaponInfo.ImpactSounds.Contains(HitResult.PhysMaterial->SurfaceType)
		                   ? WeaponInfo.ImpactSounds[HitResult.PhysMaterial->SurfaceType]
		                   : WeaponInfo.ImpactSounds[SurfaceType_Default];

	UGameplayStatics::PlaySoundAtLocation(this, Sound, HitResult.ImpactPoint);
}

void ATDSWeaponBase::PlayImpactFX(const FHitResult& HitResult) const
{
	if(!WeaponInfo.ImpactFXs.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto ParticleSystem = WeaponInfo.ImpactFXs.Contains(HitResult.PhysMaterial->SurfaceType)
		                            ? WeaponInfo.ImpactFXs[HitResult.PhysMaterial->SurfaceType]
		                            : WeaponInfo.ImpactFXs[SurfaceType_Default];

	UGameplayStatics::SpawnEmitterAtLocation(this, ParticleSystem, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation());
}

void ATDSWeaponBase::PlayMuzzleFX() const
{
	if(!WeaponInfo.MuzzleFX || !Muzzle) return;
	UGameplayStatics::SpawnEmitterAttached(WeaponInfo.MuzzleFX, Muzzle, NAME_None);
}

void ATDSWeaponBase::PlayTrailFX(const FVector& TrailEnd) const
{
	if(!WeaponInfo.MuzzleFX || !Muzzle) return;

	const auto Trail = UGameplayStatics::SpawnEmitterAtLocation(this,
	                                                            WeaponInfo.TrailFX,
	                                                            Muzzle->GetComponentLocation(),
	                                                            Muzzle->GetComponentRotation());
	Trail->SetVectorParameter(WeaponInfo.TrailEndParamName, TrailEnd);
}

void ATDSWeaponBase::SpawnImpactDecal(const FHitResult& HitResult) const
{
	if(!WeaponInfo.ImpactDecals.Num() || HitResult.PhysMaterial == nullptr) return;

	const auto DecalMaterial = WeaponInfo.ImpactDecals.Contains(HitResult.PhysMaterial->SurfaceType)
		                           ? WeaponInfo.ImpactDecals[HitResult.PhysMaterial->SurfaceType]
		                           : WeaponInfo.ImpactDecals[SurfaceType_Default];

	const auto Decal = UGameplayStatics::SpawnDecalAtLocation(this,
	                                                          DecalMaterial,
	                                                          WeaponInfo.ImpactDecalSize,
	                                                          HitResult.ImpactPoint,
	                                                          HitResult.ImpactPoint.Rotation(),
	                                                          WeaponInfo.ImpactDecalLifeTime);
	Decal->SetFadeOut(WeaponInfo.ImpactDecalLifeTime, WeaponInfo.ImpactDecalFadeoutTime);
}

void ATDSWeaponBase::SetRoundAmount(const int32 NewAmount)
{
	RoundCount = NewAmount;
	if(WeaponComponent)
	{
		WeaponComponent->OnUseRounds.Broadcast(WeaponTableName, RoundCount);
	}
}
