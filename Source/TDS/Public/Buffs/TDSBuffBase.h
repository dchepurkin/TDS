// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDSBuffBase.generated.h"

UENUM(BlueprintType)
enum class EBuffType : uint8
{
	Damage,
	Heal
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnBuffDestroySignature, class UTDSBuffBase*)

UCLASS(Abstract, NotBlueprintable)
class TDS_API UTDSBuffBase : public UObject
{
	GENERATED_BODY()

public:
	FOnBuffDestroySignature OnBuffDestroy;
	virtual void InitBuff(AActor* Target, AActor* Caster);
	virtual void DestroyBuff();
	float GetChance() const { return Chance; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff", meta=(ClampMin=0.001f, ClampMax=1.f))
	float Chance = 0.3f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff|FX")
	UParticleSystem* FX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff|FX")
	USoundBase* Sound;

	UPROPERTY()
	AActor* BuffTarget = nullptr;

	UPROPERTY()
	AActor* BuffCaster = nullptr;

	UPROPERTY()
	UParticleSystemComponent* FXComponent = nullptr;
};

UCLASS(Abstract, Blueprintable)
class TDS_API UTDSBuffOnce : public UTDSBuffBase
{
	GENERATED_BODY()

public:
	virtual void InitBuff(AActor* Target, AActor* Caster) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff")
	EBuffType BuffType = EBuffType::Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff", meta=(ClampMin = 0.f))
	float Power = 20.f;

	void ApplyBuff() const;
};

UCLASS(Abstract, Blueprintable)
class TDS_API UTDSBuffTick : public UTDSBuffOnce
{
	GENERATED_BODY()

public:
	virtual void InitBuff(AActor* Target, AActor* Caster) override;
	virtual void DestroyBuff() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff")
	float BuffTime = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff")
	float BuffTickRate = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff")
	bool AOE = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Buff", meta=(EditCondition="AOE"))
	float Radius = 200.f;

private:
	FTimerHandle LifeTimerHandle;
	FTimerHandle TickTimerHandle;

	void MakeAOE();
};
