// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSMovementComponent.h"
#include "TDSTypes.h"
#include "GameFramework/Character.h"
#include "TDSCharacter.generated.h"

class UTDSBuffComponent;
class UCameraComponent;
class UTDSSpringArmComponent;
class UTDSWeaponComponent;
class UTDSInventoryComponent;
class UTDSCharacterStatsComponent;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	explicit ATDSCharacter(const FObjectInitializer& ObjectInitializer);
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return bIsDead; }

	FORCEINLINE UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE UTDSSpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE UTDSMovementComponent* GetTDSCharacterMovement() const { return Cast<UTDSMovementComponent>(GetCharacterMovement()); }

protected:	
	UPROPERTY(EditAnywhere, Category="Character|Cursor")
	UMaterial* CursorDecalMaterial;

	UPROPERTY(EditAnywhere, Category="Character|Cursor")
	FVector CursorDecalSize = FVector(16.0f, 32.0f, 32.0f);

	UPROPERTY(EditAnywhere, Category="Character|Cursor")
	float RotationToCursorSpeed = 400.f;

	UPROPERTY(EditAnywhere, Category="Character|Camera")
	bool Flow = true;

	UPROPERTY(EditAnywhere, Category="Character|Camera")
	float CoefSmooth = 5.f;

	UPROPERTY(EditAnywhere, Category="Character|Camera")
	FRotator CameraRotation = FRotator(-75.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, Category="Character|Animation")
	TArray<UAnimMontage*> DeathAnimations;

	UFUNCTION(BlueprintImplementableEvent)
	void OnReloadStart(UAnimMontage* Animation, float ReloadTime);

	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath();

	UFUNCTION(BlueprintCallable)
	UAnimMontage* GetDeathAnim() const;

	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UTDSSpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ShellMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UTDSWeaponComponent* WeaponComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UTDSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UTDSCharacterStatsComponent* StatsComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UTDSBuffComponent* BuffComponent;

	UPROPERTY()
	UDecalComponent* CurrentCursor;

	bool bIsDead = false;

	void CursorTick(const float DeltaSeconds);
	void SetCursorTransform(const FHitResult& HitResult) const;
	void SetRotationToCursor(const float DeltaSeconds);

	void GetHitResultUnderCursor(FHitResult& HitResult) const;

	void InputAxisX(const float AxisValue);
	void InputAxisY(const float AxisValue);

	void StartFire();
	void StopFire();
	void ReloadWeapon();

	void OnReloadEnd(const FName& WeaponTableName);
	void OnMakeShoot(const FFireAnimationData& FireAnimations);
	void DropWeapon();
	bool CanFire() const;

	bool IsSprint() const;
	bool IsMoving() const;
	void OnSprintAction(const bool SptintEnabled);

	UFUNCTION()
	void OnStaminaOver();

	UFUNCTION()
	void Death();

	UFUNCTION(BlueprintCallable)
	void SetMovementState(const EMovementState NewMovementState);

	void OnChangeMovementState(const EMovementState NewMovementState);
};
