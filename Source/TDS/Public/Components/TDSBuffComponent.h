// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSBuffComponent.generated.h"

class UTDSBuffBase;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSBuffComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UTDSBuffComponent();
	void TryToAddBuff(TSubclassOf<UTDSBuffBase>& NewBuff, AActor* Caster);

protected:
private:
	TArray<UTDSBuffBase*> Buffs;

	void DestroyBuff(UTDSBuffBase* DestroyedBuff);
};
