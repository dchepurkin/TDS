// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/TDSHealthComponent.h"
#include "TDSCharacterStatsComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBrokeShieldSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldTakeDamageSignature, const float, Damage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeShieldSignature, const float, CurrentShield, const float, ShieldPercent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStaminaOverSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeStaminaSignature, const float, CurrentStamina, const float, StaminaPercent);

UCLASS()
class TDS_API UTDSCharacterStatsComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnBrokeShieldSignature OnBrokeShield;

	UPROPERTY(BlueprintAssignable)
	FOnChangeShieldSignature OnChangeShield;

	UPROPERTY(BlueprintAssignable)
	FOnShieldTakeDamageSignature OnShieldTakeDamage;

	UPROPERTY(BlueprintAssignable)
	FOnStaminaOverSignature OnStaminaOver;

	UPROPERTY(BlueprintAssignable)
	FOnChangeStaminaSignature OnChangeStamina;

	void AddShield(const float ShieldToAdd);
	void AddStamina(const float StaminaToAdd);	
	bool IsShieldBroked() const { return FMath::IsNearlyZero(CurrentShield); }
	bool IsStaminaOver() const { return FMath::IsNearlyZero(CurrentStamina); }

	void SetUsingStamina(const bool UsingEnabled);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Shield")
	float MaxShield = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Shield", meta=(ClampMin=0.1f))
	float ShieldRecoveryDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Shield", meta=(ClampMin=0.01f))
	float ShieldRecoveryRate = 0.01f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Shield", meta=(ClampMin=0.01f))
	float ShieldRecovery = 0.1f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina")
	float MaxStamina = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina", meta=(ClampMin=0.1f))
	float StaimnaRecoveryDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina", meta=(ClampMin=0.01f))
	float StaminaRecoveryRate = 0.02f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina", meta=(ClampMin=0.01f))
	float StaminaRecovery = 0.1f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina", meta=(ClampMin=0.01f))
	float StaminaUseRate = 0.01f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Stamina", meta=(ClampMin=0.01f))
	float StaminaUse = 0.1f;

	virtual void BeginPlay() override;

private:
	float CurrentShield;
	FTimerHandle ShieldRecoveryTimerHandle;

	float CurrentStamina;
	FTimerHandle StaminaRecoveryTimerHandle;
	FTimerHandle StaminaUseTimerHandle;

	virtual void TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser) override;

	void SetShield(const float NewShield);
	void UseShield(const float ShieldToUse);
	void ShieldRecoveryTick();
	void SetSheildTimerEnabled(const bool Enabled);

	void SetStamina(const float NewStamina);
	void UseStamina(const float StaminaToUse);
	void StaminaUseTick();
	void StaminaRecoveryTick();
	void SetStaminaRecoveryTimerEnabled(const bool Enabled);
	void SetStaminaUseTimerEnabled(const bool Enabled);
};
