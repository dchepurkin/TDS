// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSDropComponent.generated.h"

class ATDSPickupBase;

UENUM(BlueprintType)
enum class EDropType : uint8
{
	DT_Weapon UMETA(DisplayName = "Weapon"),
	DT_Ammo UMETA(DisplayName = "Ammo"),
	DT_HealthBox UMETA(DisplayName = "HealthBox"),
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSDropComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UTDSDropComponent();
	void Drop();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Drop")
	TMap<EDropType, TSubclassOf<ATDSPickupBase>> DropPickupClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Drop")
	TArray<FName> PickupNames;
};
