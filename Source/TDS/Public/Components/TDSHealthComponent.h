// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTakeDamageSignature, const float, Damage);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddHealthSignature, const float, AddedHealth);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeHealthSignature, const float, CurrentHealth, const float, HealthPercent);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnDeathSignature OnDeath;

	UPROPERTY(BlueprintAssignable)
	FOnChangeHealthSignature OnChangeHealth;

	UPROPERTY(BlueprintAssignable)
	FOnTakeDamageSignature OnTakeDamage;

	UPROPERTY(BlueprintAssignable)
	FOnAddHealthSignature OnAddHealth;

	UTDSHealthComponent();
	void AddHealth(const float HeathToAdd);
	bool IsFullHealth() const { return FMath::IsNearlyEqual(CurrentHealth, MaxHealth); }
	bool IsDead() const { return CurrentHealth <= 0.f; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Stats|Health")
	float MaxHealth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Stats|Health", meta=(ClampMin=0.01f))
	float DamageCoef = 1.f;

	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

private:
	float CurrentHealth;

	void SetHealth(const float NewHealth);
	void UseHealth(const float HealthToUse);
};
