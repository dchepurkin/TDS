// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSTypes.h"
#include "Components/ActorComponent.h"
#include "TDSInventoryComponent.generated.h"

class ATDSWeaponPickup;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeWeaponSignature, const int32, WeaponIndex);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeWeaponSlotSignature, const int32, ChangedSlotIndex);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeRoundsSignature, const int32, WeaponSlotIndex, const int32, Rounds);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeClipsSignature, const FName&, WeaponTableName, const int32, Clips);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnChangeWeaponSignature OnChangeWeapon;

	UPROPERTY(BlueprintAssignable)
	FOnChangeRoundsSignature OnChangeRounds;

	UPROPERTY(BlueprintAssignable)
	FOnChangeClipsSignature OnChangeClips;

	UPROPERTY(BlueprintAssignable)
	FOnChangeWeaponSlotSignature OnChangeWeaponSlot;

	UFUNCTION(BlueprintCallable)
	UTexture2D* GetWeaponIcon(const FName& WeaponTableName) const;

	UFUNCTION(BlueprintCallable)
	void GetAmmoInfo(const FName& WeaponTableName, int32& Rounds) const;

	void SetWeapon(const int32 WeaponIndex);
	bool HaveClips(const FName& WeaponTableName) const;

	bool AddAmmo(const FName& WeaponTableName, const int32 Clips);
	bool AddWeapon(const FName& WeaponTableName, const int32& Rounds);
	void DropWeapon();

	UTDSInventoryComponent();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FWeaponSlotInfo> WeaponSlots;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FAmmoSlotInfo> AmmoSlots;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* DefaultSlotIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<ATDSWeaponPickup> DropWeaponPickupClass;

	virtual void BeginPlay() override;

private:
	int32 CurrentWeaponIndex = 0;

	void InitInventory();
	bool GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const;
	void ChangeRounds(const FName& WeaponTableName, const int32 Rounds);
	void UseClip(const FName& WeaponTableName);
	int32 GetClips(const FName& WeaponTableName) const;
	bool GetEmptySlot(int32& EmptySlotIndex);
	bool IsHaveTakenWeapon(const FName& WeaponTableName);
	int32 GetWeaponSlotIndexByName(const FName& WeaponTableName);
	void TryToReload(const FName& WeaponTableName);
};
