// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDSTypes.h"
#include "TDSMovementComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnChangeMovementStateSignature, EMovementState)

UCLASS()
class TDS_API UTDSMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	FOnChangeMovementStateSignature OnChangeMovementState;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnAimAction(const bool IsAimEnabled);
	void OnWalkAction(const bool IsWalkEnabled);
	void SetSprint(const bool IsSprintEnabled);
	virtual float GetMaxSpeed() const override;

	UFUNCTION(BlueprintCallable)
	bool IsAim() const { return IsAimState; }

	UFUNCTION(BlueprintCallable)
	bool IsSprint() const;

	bool IsMoving() const { return !FMath::IsNearlyZero(GetSpeed()); }

	void SetMovementState(const EMovementState NewMovementState);
	EMovementState GetMovementState() const { return CharacterMovementState; }

	UFUNCTION(BlueprintCallable)
	float GetSpeed() const { return Velocity.Size(); }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState CharacterMovementState = EMovementState::MS_RunState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementSpeedInfo;

	virtual void BeginPlay() override;

private:
	EMovementState PrevMovementState = EMovementState::MS_RunState;
	FTimerHandle SprintTimer;
	FTimerHandle StaminaRestoreTimer;
	bool IsWalkState = false;
	bool IsRunState = true;
	bool IsSprintState = false;
	bool IsAimState = false;
};
