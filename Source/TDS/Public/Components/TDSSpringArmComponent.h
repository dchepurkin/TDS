// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "TDSSpringArmComponent.generated.h"

UCLASS()
class TDS_API UTDSSpringArmComponent : public USpringArmComponent
{
	GENERATED_BODY()

public:
	void ChangeCameraHeight(const float AxisValue);
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY(EditAnywhere, Category="Camera")
	float MinArmLength = 600.f;

	UPROPERTY(EditAnywhere, Category="Camera")
	float MaxArmLength = 1500.f;

	UPROPERTY(EditAnywhere, Category="Camera")
	float ArmLengthChangeStep = 200.f;

	UPROPERTY(EditAnywhere, Category="Camera", DisplayName="Slide With Smooth")
	bool bSlideWithSmooth = true;

	UPROPERTY(EditAnywhere, Category="Camera", meta=(EditCondition=bSlideWithSmooth))
	float ArmLengthSlideSpeed = 600.f;

	virtual void BeginPlay() override;

private:
	float CameraHeightChangeTo;

	void ArmLengthSlide(const float DeltaTime);
	float GetNewArmLength(const float StepCoef) const;
};
