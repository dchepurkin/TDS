// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSTypes.h"
#include "Components/ActorComponent.h"
#include "TDSWeaponComponent.generated.h"

class UDataTable;
class ATDSWeaponBase;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnReloadStartSignature, UAnimMontage* Animation, float ReloadTime);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnMakeShootSignature, const FFireAnimationData&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnReloadEndSignature, const FName& WeaponTableName);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnUseRoundsSignature, const FName& WeaponTableName, const int32 Rounds);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDSWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	FOnMakeShootSignature OnMakeShoot;
	FOnReloadStartSignature OnReloadStart;
	FOnReloadEndSignature OnReloadEnd;
	FOnUseRoundsSignature OnUseRounds;

	UTDSWeaponComponent();

	void StartFire() const;
	void StopFire() const;
	void ReloadWeapon() const;
	bool IsReload() const;
	void TryToReload(ATDSWeaponBase* Weapon) const;

	void GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const;
	bool GetProjectileInfo(const FName& ProjectileTableName, FProjectileInfo& ProjectileInfo) const;
	ATDSWeaponBase* CreateWeapon(const TSubclassOf<ATDSWeaponBase> WeaponClass, const int32& Rounds) const;
	bool SetCurrentWeapon(ATDSWeaponBase* Weapon);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Weapon|Data Tables")
	UDataTable* ProjectileDataTable;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	USceneComponent* AttachToComponent;

	UPROPERTY()
	ATDSWeaponBase* CurrentWeapon = nullptr;

	FName GetWeaponSocketName(const FName& WeaponTableName) const;
	bool CanChangeWeapon() const;
};
