// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include <stdbool.h>

#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TDSTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	MS_AimState UMETA(DisplayName = "Aim"),
	MS_WalkState UMETA(DisplayName = "Walk"),
	MS_RunState UMETA(DisplayName = "Run"),
	MS_SprintState UMETA(DisplayName = "Sprint"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	TMap<EMovementState, float> Speed{
		{EMovementState::MS_AimState, 200.f},
		{EMovementState::MS_RunState, 600.f},
		{EMovementState::MS_SprintState, 800.f},
		{EMovementState::MS_WalkState, 350.f},
	};
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Dispersion")
	float DispersionMin = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Dispersion")
	float DispersionMax = 1.f;
};

USTRUCT(BlueprintType)
struct FFireAnimationData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animation")
	UAnimMontage* FireAnimation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animation")
	UAnimMontage* AimFireAnimation = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
	bool bDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class ATDSWeaponBase> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* WeaponMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Socket")
	FName AttachToSocketName = "WeaponSocket";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Is Projectile Weapon", meta=(ToolTip="True - weapon use projectile, False - use trace damage"))
	bool bIsProjectileWeapon = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta=(EditCondition="!bIsProjectileWeapon", ClampMin=0.f))
	float Damage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta=(EditCondition="!bIsProjectileWeapon", ClampMin=100.f))
	float TraceDistance = 2000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta=(EditCondition="!bIsProjectileWeapon"))
	TSubclassOf<class UTDSBuffBase> Buff = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State", meta=(ClampMin=0.01f))
	float RateOfFire = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State", meta=(ClampMin=0.1f))
	float ReloadTime = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State", meta=(ClampMin=1))
	int32 MaxRound = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="State", meta=(ClampMin=1))
	int32 RoundPerShoot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Dispersion")
	TMap<EMovementState, FWeaponDispersion> Dispersion{
		{EMovementState::MS_AimState, FWeaponDispersion()},
		{EMovementState::MS_RunState, FWeaponDispersion()},
		{EMovementState::MS_WalkState, FWeaponDispersion()},
	};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	USoundBase* FireSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	USoundBase* EmptyAmmoSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	USoundBase* ReloadSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Sound")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> ImpactSounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	UParticleSystem* MuzzleFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX", meta=(EditCondition="!bIsProjectileWeapon"))
	UParticleSystem* TrailFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX", meta=(EditCondition="!bIsProjectileWeapon"))
	FName TrailEndParamName = "BeamEnd";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> ImpactFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> ImpactDecals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	float ImpactDecalLifeTime = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	float ImpactDecalFadeoutTime = 1.5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	FVector ImpactDecalSize{30.f, 30.f, 30.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Projectile", meta=(EditCondition="bIsProjectileWeapon"))
	TSubclassOf<class ATDSProjectileBase> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animation")
	FFireAnimationData FireAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animation")
	UAnimMontage* ReloadAnimation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Drop")
	UStaticMesh* MagazineMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Drop")
	UStaticMesh* SleeveMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Drop")
	TSubclassOf<class ATDSSleeve> DropActorClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Inventory")
	UTexture2D* WeaponIcon = nullptr;
};

USTRUCT(BlueprintType)
struct FProjectileInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
	bool bDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Visual")
	UStaticMesh* Mesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Visual")
	UParticleSystem* TrailFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta=(ClampMin=0.f))
	float Damage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage")
	TSubclassOf<class UTDSBuffBase> Buff = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", DisplayName="Radial Damage")
	bool bRadialDamage = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta=(ClampMin=1.f, EditCondition="bRadialDamage"))
	float DamageRadius = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Speed", meta=(ClampMin=0.f))
	float InitSpeed = 2000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> ImpactSounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> ImpactFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> ImpactDecals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	float ImpactDecalLifeTime = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	float ImpactDecalFadeoutTime = 1.5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="FX")
	FVector ImpactDecalSize{30.f, 30.f, 30.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin=0.1f))
	float LifeTime = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Can Bounce")
	bool bCanBounce = false;
};

USTRUCT(BlueprintType)
struct FAmmoSlotInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponTableName = NAME_None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Infinity")
	bool bInfinity = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(EditCondition="!bInfinity"))
	int32 ClipsAmount = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* AmmoIcon = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponSlotInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponTableName = NAME_None;

	UPROPERTY()
	ATDSWeaponBase* Weapon = nullptr;
};

UCLASS()
class TDS_API UTDSTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
};
