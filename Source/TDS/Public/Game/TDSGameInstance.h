// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSTypes.h"
#include "Engine/GameInstance.h"
#include "TDSGameInstance.generated.h"

class UDataTable;

UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	bool GetWeaponInfo(const FName& WeaponTableName, FWeaponInfo& WeaponInfo) const;
	void GetProjectileInfo(const FName& ProjectileTableName, FProjectileInfo& ProjectileInfo) const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Tables")
	UDataTable* WeaponDataTable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Tables")
	UDataTable* ProjectileDataTable;
};
