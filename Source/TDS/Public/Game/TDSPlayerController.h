// Created by Dmitriy Chepurkin, All Right Reserved
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPauseGameSignature, bool, IsPaused);

UCLASS()
class ATDSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnPauseGameSignature OnPauseGame;

	UFUNCTION(BlueprintCallable)
	void TogglePause();
	
	ATDSPlayerController();	

protected:
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;
};
