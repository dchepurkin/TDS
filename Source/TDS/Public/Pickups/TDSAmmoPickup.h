// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Pickups/TDSPickupBase.h"
#include "TDSAmmoPickup.generated.h"

UCLASS(Blueprintable)
class TDS_API ATDSAmmoPickup : public ATDSPickupBase
{
	GENERATED_BODY()

public:
	void SetClips(const int32 NewClips) { Clips = NewClips; }
	void SetWeaponName(const FName& WeaponName) { WeaponTableName = WeaponName; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	FName WeaponTableName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	int32 Clips = 3;

	virtual void TryToTakePickup(AActor* Player) override;
};
