// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Pickups/TDSPickupBase.h"
#include "TDSHealthPickup.generated.h"

UCLASS(Blueprintable)
class TDS_API ATDSHealthPickup : public ATDSPickupBase
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pickup")
	float Health = 10.f;

	virtual void TryToTakePickup(AActor* Player) override;
};
