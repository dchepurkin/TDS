// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSPickupBase.generated.h"

class USphereComponent;

UCLASS(Abstract, NotBlueprintable)
class TDS_API ATDSPickupBase : public AActor
{
	GENERATED_BODY()

public:
	ATDSPickupBase();
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	USphereComponent* Collision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UParticleSystemComponent* Particle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	float RotationRate = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	USoundBase* OnTakeSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Pickup")
	UParticleSystem* OnTakeFX;

	virtual void BeginPlay() override;
	virtual void TryToTakePickup(AActor* Player);

private:
	UFUNCTION()
	void OnCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	                             AActor* OtherActor,
	                             UPrimitiveComponent* OtherComp,
	                             int32 OtherBodyIndex,
	                             bool bFromSweep,
	                             const FHitResult& SweepResult);

	void RotationTick(const float DeltaSeconds) const;
};
