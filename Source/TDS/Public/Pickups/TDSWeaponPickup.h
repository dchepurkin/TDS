// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Pickups/TDSPickupBase.h"
#include "TDSWeaponPickup.generated.h"

UCLASS(Blueprintable)
class TDS_API ATDSWeaponPickup : public ATDSPickupBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	void SetWeaponName(const FName& WeaponName) { WeaponTableName = WeaponName; }
	void SetRounds(const int32& NewRounds) { Rounds = NewRounds; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pickup")
	FName WeaponTableName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Pickup")
	int32 Rounds = 0;

	virtual void TryToTakePickup(AActor* Player) override;

private:
	UStaticMesh* GetWeaponMesh() const;
};
