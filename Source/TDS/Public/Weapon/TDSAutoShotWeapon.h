// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/TDSWeaponBase.h"
#include "TDSAutoShotWeapon.generated.h"

UCLASS(Abstract)
class TDS_API ATDSAutoShotWeapon : public ATDSWeaponBase
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;
	virtual void StopFire() override;

private:
	FTimerHandle AutoShotTimerHandle;
	
	void AutoShot();
	void SetAutoShotTimerEnabled(const bool Enabled);
};
