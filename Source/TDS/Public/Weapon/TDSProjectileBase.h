// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSTypes.h"
#include "GameFramework/Actor.h"
#include "TDSProjectileBase.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS(Abstract)
class TDS_API ATDSProjectileBase : public AActor
{
	GENERATED_BODY()

public:
	ATDSProjectileBase();

	void InitProjectile(const bool bUseImportProjectileInfo, const FProjectileInfo& NewProjectileInfo, AActor* IgnoredActor);
	const FName& GetTableName() const { return ProjectileTableName; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	USphereComponent* ProjectileCollision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UParticleSystemComponent* FX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Projectile")
	FName ProjectileTableName = NAME_None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Projectile")
	FProjectileInfo ProjectileInfo;

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	                    AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex,
	                    bool bFromSweep,
	                    const FHitResult& SweepResult);

	void PlayImpactSound(const FHitResult& HitResult) const;
	void PlayImpactFX(const FHitResult& HitResult) const;
	void SpawnImpactDecal(const FHitResult& HitResult) const;
	void MakeRadialDamage();
	void MakePointDamage(AActor* DamagedActor);
	void TryToApplyBuff(AActor* Actor);
	void TryToApplyBuff(TArray<AActor*>& Actors);
};
