// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSSleeve.generated.h"

class UProjectileMovementComponent;

UCLASS()
class TDS_API ATDSSleeve : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDSSleeve();
	void SetStaticMesh(UStaticMesh* NewMesh) const;

protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Components")
	UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta=(ClampMin=0.1f))
	float LifeTime = 3.f;
	
	virtual void BeginPlay() override;
};
