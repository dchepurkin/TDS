// Created by Dmitriy Chepurkin, All Right Reserved

#pragma once

#include "CoreMinimal.h"
#include "TDSTypes.h"
#include "GameFramework/Actor.h"
#include "TDSWeaponBase.generated.h"

class UArrowComponent;
class UTDSWeaponComponent;

UCLASS(Abstract)
class TDS_API ATDSWeaponBase : public AActor
{
	GENERATED_BODY()

public:
	ATDSWeaponBase();
	virtual void StartFire();
	virtual void StopFire() {}
	void SetVisibility(const bool Visibility) const;
	void StartReload();
	const FWeaponInfo& GetWeaponInfo() const { return WeaponInfo; }
	const FName& GetWeaponTableName() const { return WeaponTableName; }
	bool IsReloading() const { return bIsReloading; }
	bool IsClipEmpty() const;
	bool IsAmmoEmpty() const;
	int32 GetRoundCount() const { return RoundCount; }
	void SetRoundAmount(const int32 NewAmount);
	int32 GetRoundsInClip() const { return WeaponInfo.MaxRound; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Components)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Components)
	UArrowComponent* Muzzle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Components)
	UArrowComponent* SleeveDropPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Components)
	UArrowComponent* MagazineDropPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Weapon)
	FName WeaponTableName = NAME_None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Weapon)
	FWeaponInfo WeaponInfo;

	virtual void BeginPlay() override;
	virtual void MakeShoot();
	bool CanFire() const;
	void StartCooldown();
	void PlaySound(USoundBase* Sound) const;
	void PlayMuzzleFX() const;
	void PlayTrailFX(const FVector& TrailEnd) const;
	void UseRound(const int32 Count);

private:
	FTimerHandle ReloadTimerHandle;
	FTimerHandle CooldownFireTimerHandle;
	bool bIsReloading = false;
	bool bInCooldown = false;
	int32 RoundCount = 0;

	UPROPERTY()
	UTDSWeaponComponent* WeaponComponent;

	bool CanReload() const;

	void OnReloadFinished();
	void PlayImpactSound(const FHitResult& HitResult) const;
	void PlayImpactFX(const FHitResult& HitResult) const;
	void SpawnImpactDecal(const FHitResult& HitResult) const;
	void OnFireCooldownEnd();

	void CreateProjectile();
	void MakeTraceShot();
	void TryToApplyBuff(AActor* Actor);

	int32 GetRoundPerShoot() const;
	void GetProjectileTransform(FTransform& Transform);
	void GetMuzzleTransform(FTransform& Transform) const;
	float GetConeHalfAngle();

	void DropSleeve() const;
	void DropMagazine() const;
};
