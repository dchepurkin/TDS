// Created by Dmitriy Chepurkin, All Right Reserved

using UnrealBuildTool;

public class TDS : ModuleRules
{
	public TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });
        PublicIncludePaths.AddRange(new string[]
        {
	        "TDS",
			"TDS/Public",
			"TDS/Public/Buffs",
			"TDS/Public/Pickups",
			"TDS/Public/Components",
			"TDS/Public/Game",
			"TDS/Public/Character",
			"TDS/Public/FunctionLibs",
        });
	}
}
