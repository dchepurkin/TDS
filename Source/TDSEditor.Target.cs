// Created by Dmitriy Chepurkin, All Right Reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class TDSEditorTarget : TargetRules
{
	public TDSEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("TDS");
	}
}
